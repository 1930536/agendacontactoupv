 

import java.util.InputMismatchException;
import java.util.Scanner;

public class AgendaContacto {

    public static void main(String[] args) {

        Scanner sn = new Scanner(System.in);
        sn.useDelimiter("\n");
        boolean salir = false;
        int opcion; //Guardaremos la opcion del usuario

        Agenda agendaTelefonica = new Agenda(3);
        String nombre;
        int telefono;

        Contacto c;

        while (!salir) {
            System.out.println("**************************");
            System.out.println("*   AGENDA DE WHATSAPP   *");
            System.out.println("**************************");
            System.out.println("1. Añadir nuevo contacto");
            System.out.println("2. Listar contactos");
            System.out.println("3. Buscar contactos");
            System.out.println("4. Contacto existente");
            System.out.println("5. Eliminar contacto");
            System.out.println("6. Contactos disponibles");
            System.out.println("7. Agenda llena");
            System.out.println("8. Salir");
            try {
                System.out.println("Escribe una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:

                        //Pido valores para nombre y telefono
                        System.out.println("Escribe un nombre");
                        nombre = sn.next();

                        System.out.println("Escribe un telefono");
                        telefono = sn.nextInt();

                        //Creo el contacto donde solo agrego nombre y telefono
                        c = new Contacto(nombre, telefono);

                        agendaTelefonica.aniadirContacto(c);

                        break;
                    case 2:

                        agendaTelefonica.listarContactos();

                        break;
                    case 3:

                        //pido el nombre del contacto
                        System.out.println("Escribe un nombre");
                        nombre = sn.next();

                        agendaTelefonica.buscarPorNombre(nombre);

                        break;
                    case 4:

                        //pido el nombre del contacto
                        System.out.println("Escribe un nombre");
                        nombre = sn.next();

                        //Creo el contacto auxiliar
                        c = new Contacto(nombre, 0);

                        if (agendaTelefonica.existeContacto(c)) {
                            System.out.println("Existe contacto");
                        } else {
                            System.out.println("No existe contacto");
                        }

                        break;
                    case 5:

                        //pido el nombre del contacto
                        System.out.println("Escribe un nombre");
                        nombre = sn.next();

                        //Creo el contacto auxiliar
                        c = new Contacto(nombre, 0);

                        agendaTelefonica.eliminarContacto(c);

                        break;
                    case 6:

                        System.out.println("Hay " + agendaTelefonica.huecosLibre() + " contactos libre");

                        break;
                    case 7:

                        //como devuelve un booleano, lo meto en una sentencia if
                        if (agendaTelefonica.agendaLlena()) {
                            System.out.println("La agenda esta llena");
                        } else {
                            System.out.println("Aun tienes espacio para ingresar contactos");
                        }

                        break;
                    case 8:
                        salir = true;
                        break;
                    default:
                        System.out.println("Numero invalido, vuelva a intentarlo mas tarde");
                }

            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }

        }

    }

}
